This is python visualize application. It is used to visualize data in both frequency and time domain.
This application was made especially for the graduation project to visualize subjects data.

You will find a sample folder "DataToRun" containing data for 2 subjects for 2 days.

How to run:

1- you need python3/ipython3.
2- Numpy, scipy, matplotlib for python3/ipython3.

How it works:

1- Choose the folder that contains the data (in this case you will find it in the attached folder under the name "DataToRun").
2- From the menu bar, choose the Subject, Day pair from the Subjects menu.
3- Choose time domain or frequency domain  (square FFT coefficients) radio button.
4- Pick the start and end intervals for both time and frequency.
5- Click Save. 
6- Click Plot.
7- If you want to compare multiple plots:
	a- Select button "addNewUser" which will add new tab.
	b- Do same steps done before till step (5).
	c- Select "addToPlotAll" to be added.
	d- Click "plotAll".

 
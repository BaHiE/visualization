import refactored_library as rlib
from functools import partial
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from matplotlib import style
import numpy as np
import tkinter.ttk as ttk
import tkinter as tk
from tkinter import *
from scipy.signal import lfilter, ellip
from tkinter.filedialog import askdirectory
from tkinter import messagebox

LARGE_FONT= ("Verdana", 12)
style.use("ggplot")
tabs = []
currentChosenUser = []
currentDayUser = []
currentChosenUser.append([])
currentDayUser.append([])
selectedUsers = []
selectedUsers.append([])
selectedTabs = set()
tabChecked = []
tabChecked.append([])


def selectUser(usr, day):
    global currentChosenUser
    global currentDayUser
    tabIndex = notes.index(notes.select())
    currentChosenUser[tabIndex] = usr
    currentDayUser[tabIndex] = day
    userName = idMAPsubj[usr].split('_')[0]
    
    
    notes.insert(notes.select(), tabIndex, text = userName + "_Day#" + str(day+1))
    
class SelectedUser(object):
    def __init__(self, sT, eT, sF, eF, day, userid):
        self.sT     = max(int(sT), 0)
        self.eT     = int(eT)
        self.sF     = max(int(sF), 0)
        self.eF     = int(eF)
        self.day    = day
        self.userid = userid

    def prepareData(self):
        data = subject_day_second_sample[self.userid][self.day][self.sT:self.eT]
       
        temp = []
        for i in range(len(data)):
            temp.extend(data[i])
        return temp

    def getDataToPlotTDOMAIN(self):
        sampling_frequency = 512
        nyq_sampling_frequency = sampling_frequency/2
        data = self.prepareData()
        
        bb, aa = ellip(4, 3, 40, [1.0*self.sF/nyq_sampling_frequency,  1.0*self.eF/nyq_sampling_frequency], 'bandpass')
        x_axis_time = (np.array(list(range(0, len(data))))+512*self.sT)/512.0
        return x_axis_time, lfilter(bb, aa, data)

    def getDataToPlotFDOMAIN(self):
        sampling_frequency = 512
        data = self.prepareData()

        fourier_coef = np.fft.fft(np.array(data))
        freq = np.fft.fftfreq(len(fourier_coef), d=1.0/sampling_frequency)

        square_absolute_FT_features = []
        x_axis_freq = []

        print(self.sF)
        for f in range(int(len(fourier_coef)/2+1)):
            if freq[f] >= self.sF and freq[f] <= self.eF:
                square_absolute_FT_features.append((fourier_coef[f]*fourier_coef[f].conjugate()).real)
                x_axis_freq.append(freq[f])

        return x_axis_freq, square_absolute_FT_features


    

class SeaofBTCapp(tk.Tk):
    
    def __init__(self, *args, **kwargs):
        global notes
        tk.Tk.__init__(self, *args, **kwargs)
        
        self.initUI()
        globalWidgets = tk.Label(self, font=LARGE_FONT, height="100", width="100")
        globalWidgets.pack(anchor= N)
        
        global var 
        var = IntVar()

        TDR = Radiobutton(globalWidgets, text="TIME DOMAIN", variable=var, value=1).grid(row=1, column=1, sticky=W)

        FDR = Radiobutton(globalWidgets, text="FREQ DOMAIN", variable=var, value=2).grid(row=1, column=3, sticky=W)
        
        B2 = Button(globalWidgets, text="AddNewUser", command=self.onAddNewUser).grid(row=2, column=2, sticky=W)
        
        B4 = Button(globalWidgets, text="PlotALL", command=self.onPlotAll).grid(row=3, column=2)

        self.note = ttk.Notebook(self, name='notebook')
        notes = self.note
        global checkCmd
        checkCmd = []
        
        global sTimeT
        sTimeT = []
        
        global eTimeT
        eTimeT = []
        
        global sFreqT
        sFreqT = []
        
        global eFreqT
        eFreqT = []
        
        self.add(self.note, "tab one")
        
    def add(self, note, tabTitle):
        tab1 = Frame(note)
        
        parentlabel = tk.Label(tab1, font=LARGE_FONT, height="800", width="400")
        parentlabel.pack(anchor=W, side="left")

       

        sTimeL = tk.Label(parentlabel, text="Start Time", font=LARGE_FONT).grid(row=1, column=1, sticky=W)
        
        sTimeT.append(StringVar())
        Entry(parentlabel, textvariable=sTimeT[len(sTimeT)-1]).grid(row=1, column=2, sticky=W)

        eTimeL = tk.Label(parentlabel, text="End Time", font=LARGE_FONT).grid(row=1, column=4, sticky=W)
        
        eTimeT.append(StringVar())
        Entry(parentlabel, textvariable=eTimeT[len(eTimeT)-1]).grid(row=1, column=5, sticky=W)

        sFreqL = tk.Label(parentlabel, text="Start Freq", font=LARGE_FONT).grid(row=2, column=1, sticky=W)
        
        sFreqT.append(StringVar())
        Entry(parentlabel, textvariable=sFreqT[len(sFreqT)-1]).grid(row=2, column=2, sticky=W)

        eFreqL = tk.Label(parentlabel, text="End Freq", font=LARGE_FONT).grid(row=2, column=4, sticky=W)
       
        eFreqT.append(StringVar())
        Entry(parentlabel, textvariable=eFreqT[len(eFreqT)-1]).grid(row=2, column=5, sticky=W)

        B1 = Button(parentlabel, text="PLOT", command=self.onPlot).grid(row=5, column=10, sticky=E)
                
        B3 = Button(parentlabel, text="Save", command=self.onSave).grid(row=5, column=1, sticky=W)
           
        checkCmd.append(IntVar())
        checkCmd[len(checkCmd)-1].set(0)
        B5 = Checkbutton(parentlabel, text="Add to plotALL", variable=checkCmd[len(checkCmd)-1], onvalue=1, offvalue=0, command=self.cb).grid(row=1, column=10, sticky=W)

        note.add(tab1, text = tabTitle)
        note.pack()
        
        
    def cb(self):
        tabIndex = notes.index(notes.select())
        checked = checkCmd[tabIndex].get()
        print(checked)
        if checked == 1:
            selectedTabs.add(tabIndex)
        elif (checked == 0) and (tabIndex in selectedTabs):
            selectedTabs.remove(tabIndex)
        
    def onSave(self):
        self.update()
        try:
            tabIndex = notes.index(notes.select())
            sTime = sTimeT[tabIndex].get()
            eTime = eTimeT[tabIndex].get()
            sFreq = sFreqT[tabIndex].get()
            eFreq = eFreqT[tabIndex].get()
            
            newUser = SelectedUser(sTime, eTime, sFreq, eFreq, currentDayUser[tabIndex], currentChosenUser[tabIndex])
            
            if currentChosenUser[tabIndex] == [] or currentDayUser[tabIndex] == []:
                tk.messagebox.showwarning(
                    "Incorrect input",
                    "Select subject and day first"
                )
            else:
                selectedUsers[tabIndex] = newUser
                
        except:
           tk.messagebox.showwarning(
                "Incorrect input",
                "Time should be between 0 and 119, Freq should be between 0 and 100"
            )
           pass
       
    def initUI(self):
        self.title("Plot")
        self.geometry('800x200+100+100')
        menubar = Menu(self)
        self.config(menu=menubar)
        
        fileMenu = Menu(menubar)
        fileMenu.add_command(label="Exit", command=self.onExit)
        menubar.add_cascade(label="File", menu=fileMenu)
        
        self.update()
        directoryPath = askdirectory()
        global subject_day_second_sample
        global idMAPsubj 
        subject_day_second_sample, idMAPsubj = rlib.acquire_dataset(directoryPath)
        
        subjectMenu = Menu(menubar)

        for i in range(len(subject_day_second_sample)):
            daySubMenu = Menu(subjectMenu)
            for j in range(len(subject_day_second_sample[i])):
                daySubMenu.add_command(label="Day #"+str(j+1), command=partial(selectUser, i, j))
                
            subjectMenu.add_cascade(label=idMAPsubj[i], menu=daySubMenu, underline=0)

        menubar.add_cascade(label="Subjects", menu=subjectMenu)


        
    def onAddNewUser(self):
        selectedUsers.append([])
        currentChosenUser.append([])
        currentDayUser.append([])
        tabChecked.append([])
        self.add(self.note, "User name?")
    
    def onPlotAll(self):
        self.update()
        domain = var.get()
        selectToPlot = []
        for idx in selectedTabs:
            selectToPlot.append(selectedUsers[idx])
        if(len(selectToPlot) == 0):
            tk.messagebox.showwarning(
                "Error",
                "Please save then check <Add to plotALL>"
            )
        else:
            window = tk.Toplevel()
            self.newWindow(window, domain, selectToPlot)
        
    def onPlot(self):
        self.update()
        domain = var.get()
        tabIndex = notes.index(notes.select())
        if(selectedUsers[tabIndex] == []):
            tk.messagebox.showwarning(
                "Error",
                "Please save first"
            )
        else:
            window = tk.Toplevel()
            self.newWindow(window, domain, [selectedUsers[tabIndex]])

        
    def newWindow(self, window, domain, selectedData):
        f = Figure(figsize=(5,5), dpi=100)
        a = f.add_subplot(111)
        container = tk.Frame(window)
        container.pack(fill="both", expand = True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
        a.clear()
        if domain == 2:
            for i in range(len(selectedData)):
                xlist, ylist = selectedData[i].getDataToPlotFDOMAIN()
                a.plot(xlist, ylist)
        else:
            for i in range(len(selectedData)):
                xlist, ylist = selectedData[i].getDataToPlotTDOMAIN()
                a.plot(xlist, ylist)
        
        label = tk.Label(window, text="PLOT", font=LARGE_FONT)
        label.pack(pady=10,padx=10)

        canvas = FigureCanvasTkAgg(f, window)
        canvas.show()
        canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

        toolbar = NavigationToolbar2TkAgg(canvas, window)
        toolbar.update()
        canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        
    def onExit(self):
        self.quit()


    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()
    
subject_day_second_sample = []
idMAPsubj = {}

def main():
    app = SeaofBTCapp()
    app.mainloop()

if __name__ == '__main__':
    main()

import numpy as np
import math
import sys

from scipy.signal import lfilter, ellip
from scipy import signal
from os import listdir
from os.path import isfile, join
# from statsmodels.tsa.ar_model import AR
from scipy.signal import ellip, filtfilt


######################
### HELPER METHODS ###
######################

sampling_frequency = 512

nyq_sampling_frequency = sampling_frequency/2

bDelta, aDelta = ellip(4, 3, 40, [                        0.0,  4.0/nyq_sampling_frequency], 'bandpass')
bTheta, aTheta = ellip(4, 3, 40, [ 4.0/nyq_sampling_frequency,  7.0/nyq_sampling_frequency], 'bandpass')
bAlpha, aAlpha = ellip(4, 3, 40, [ 7.0/nyq_sampling_frequency, 13.0/nyq_sampling_frequency], 'bandpass')
bBeta,  aBeta  = ellip(4, 3, 40, [13.0/nyq_sampling_frequency, 20.0/nyq_sampling_frequency], 'bandpass')
bGamma, aGamma = ellip(4, 3, 40, [20.0/nyq_sampling_frequency, 50.0/nyq_sampling_frequency], 'bandpass')


user_id_map = {}
# Loop on all text files in the given directory to get data set for give users number
def acquire_dataset(dirname):
    # Get all the files in the given directory
    participant_files = [f for f in listdir(dirname) if isfile(join(dirname, f))]

    # 3D Array, 1st dimension: user id, 2nd dimension: one image, 3rd dimension: raw EEG samples (2048)
    total_data = []

    id_map = {}
    usersNumber = 0
    for i in range(len(participant_files)):
        if participant_files[i][0] != '.':     # To ignore hidden files beginning with '.'
            participant_id, each_image_data = create_data_per_participant(dirname+"/"+participant_files[i])
            #print(participant_files[i])
            if participant_id in id_map:
                participant_id = id_map[participant_id]
            else:
                # print("id " + str(usersNumber) + " name "+str(participant_id))
                user_id_map[usersNumber] = participant_files[i]
                id_map[participant_id] = usersNumber
                participant_id = usersNumber
                usersNumber += 1
                total_data.append([])
            #ADDED
            temp = []
            for val in range(len(each_image_data)):
                temp.append([])
                for k in (each_image_data[val]):
                    temp[val].append(k)

            total_data[participant_id].append(temp)
            #ADDED

    return total_data, user_id_map



# Reads the input file and read the EEG raw data signal for a certain user performing an experiment
# The experiment had 5 rounds, each round has 10 seconds of breathing we take the middle 5 seconds 
def create_data_per_participant(fname):
    # Read EEG raw data file
    file_content = open(fname, 'r')
    
    # Get the participant id from the file name
    participant_info = fname.split('_')
    participant_id = participant_info[0]
    
    # Read the content of this file (50 line, line for each second)
    data = file_content.read()
    
    # Split on '\n' to get each line (corresponding to one second of experiment)
    array = data.split('\n')
    
    # 2D integer array, each second and its corresponding 512 raw EEG samples
    user_total_data = []
    # Each second has 512 raw EEG samples separarted by spaces
    # so we loop on lines to get samples corresponding to this seconds and map them to integers
    for i in range(len(array)-1):                # loop till len(array)-1 because last line is empty new line
        temp_array = array[i].split(' ')
        if temp_array[len(temp_array)-1] == "":
            temp_array = map(int, temp_array[:-1])   # till -1 bec. last element is line is empty
        else:
            temp_array = map(int, temp_array[:]) 
        user_total_data.append(temp_array)
    
    return participant_id, user_total_data

def load_map(file_path):
    file_content = open(file_path, 'r')
    id_map = {}
    for line in file_content:
        line_info = line.split(" ")
        cur_name  = line_info[0]
        cur_id    = int(line_info[1])
        id_map[cur_name] = cur_id
    return id_map

def get_train_id(test_path, train_path):
    id_map = load_map(train_path)
    file_content = open(test_path, 'r')
    id_arr = []
    for line in file_content:
        line_info = line.split(" ")
        cur_name  = line_info[0]
        if cur_name in id_map:
            id_arr.append(id_map[cur_name])
        else:
            id_arr.append(-1)
    return id_arr

# def feature_extraction(subject_segment_channel_sample):
    
#     if DEVICE == EMOTIV:
#         SETOFALLOWEDCHANNELS = constructChannelSet(CHOSENCHANNELS)

#     b, a = ellip(4, 3, 40, [2.0/nyq_sampling_frequency, min(100.0/nyq_sampling_frequency, 1)], 'bandpass')

#     subject_segment_feature = []

#     for subject in range(len(subject_segment_channel_sample)):
#         subject_segment_feature.append([])
#         for segment in range(len(subject_segment_channel_sample[subject])):
#             subject_segment_feature[subject].append([])
#             SP_channel = []
#             for channel in range(len(subject_segment_channel_sample[subject][segment])):

#                 # If emotiv: Check if this channel is included or not.
#                 if not channel in SETOFALLOWEDCHANNELS:
#                     SP_channel.append([])
#                     continue


#                 # Apply Elliptic Filter in Forward and Reverse directions
#                 subject_segment_channel_sample[subject][segment][channel] = filtfilt(b, a, 
#                                                                 subject_segment_channel_sample[subject][segment][channel])

#                 if FEATURE_CLASS != WELCHONLY and FEATURE_CLASS != FFTONLY and FEATURE_CLASS != STFTONLY:
#                     # Apply Autoregression model and add the AR coeffients to the feature vector
#                     AR_features = get_AR_features(subject_segment_channel_sample[subject][segment][channel])
#                     subject_segment_feature[subject][segment].extend(AR_features)  #  +6 features

#                 # if FEATURE_CLASS == OLD_FEATURES:
#                 #     # Apply Fourier Transformation and add the square magnitude of the fourier coef to the feature vector 
#                 #     PSD_features = get_Power_Spectral_Density(subject_segment_channel_sample[subject][segment][channel])
#                 #     subject_segment_feature[subject][segment].extend(PSD_features) # +65 features for emotiv, +257 for mindwave

#                 #Integrate the spectral density over the delta, theta, alpha, beta and gamma frequency ranges
#                 SP_features = get_Spectral_Power(subject_segment_channel_sample[subject][segment][channel])
#                 #     subject_segment_feature[subject][segment].extend(SP_features)  #  +5 features

#                 # Store the SP features to be used in IHPD
#                 SP_channel.append(SP_features)

#                 if FEATURE_CLASS == FFTONLY or FEATURE_CLASS == FFTandAR:
#                     # Apply Fourier Transformation and add the square magnitude of the fourier coef to the feature vector 
#                     PSD_features = get_Power_Spectral_Density(subject_segment_channel_sample[subject][segment][channel])
#                     subject_segment_feature[subject][segment].extend(PSD_features) # +65 features for emotiv, +257 for mindwave

#                 if FEATURE_CLASS == STFTONLY or FEATURE_CLASS == STFTandAR:
#                     # Apply Short Time Fourier Transformation and add the square magnitude of the fourier coef to the feature vector 
#                     STFT_features = get_STFT(subject_segment_channel_sample[subject][segment][channel])
#                     subject_segment_feature[subject][segment].extend(STFT_features)

#                 if  FEATURE_CLASS == WELCHONLY or FEATURE_CLASS == WELCHandAR:
#                     # APPLY WELCH POWER and add the power of each frequency to the feature vector
#                     WELCH_features = get_WELCH(subject_segment_channel_sample[subject][segment][channel])
#                     subject_segment_feature[subject][segment].extend(WELCH_features)

#             if DEVICE == EMOTIV:
#                 # Compute interhemispheric spectral power difference and add it to the feature vector
#                 IHPD_features, power_diff_matrix = get_Interhemispheric_Power_Difference(SP_channel)
#                 subject_segment_feature[subject][segment].extend(IHPD_features)

#                 if len(SETOFALLOWEDCHANNELS) == 14:
#                     # Compute interhemispheric channel linear complexity and add it to the feature vector
#                     IHLC_features = getInterhemispheric_Channel_Linear_Complexity(power_diff_matrix)
#                     subject_segment_feature[subject][segment].extend(IHLC_features)

#     subject_block_segment_feature = []

# ################ NUMBER SEGMENTS PER BLOCK ################
#     if DEVICE == MINDWAVE:                                #
#         segments_per_block = 10 / SEGMENT_SIZE            #
#     elif DEVICE == EMOTIV:                                #
#         segments_per_block = 15 / SEGMENT_SIZE            #
# ###########################################################

#     for subject in range(len(subject_segment_feature)):
#         subject_block_segment_feature.append([])
#         for block in range(0, len(subject_segment_feature[subject]), segments_per_block):
#             subject_block_segment_feature[subject].append(subject_segment_feature[subject][block:block+segments_per_block])

#     return subject_block_segment_feature

# ###################################
# ### FEATURE CALCULATION METHODS ###
# ###################################
# def get_AR_features(segment):
#     if AR_N == 0:
#         return []
#     model = AR(segment)
#     result = model.fit(maxlag=AR_N, trend='nc')
#     return result.params

# def get_Power_Spectral_Density(segment):
#     fourier_coef = np.fft.fft(np.array(segment))
#     freq         = np.fft.fftfreq(len(fourier_coef), d=1.0/sampling_frequency)

#     square_absolute_FT_features = []
    
#     for f in range(len(fourier_coef)/2+1):
#         if freq[f] >= LOW_FREQ and freq[f] <= HIGH_FREQ:
#             square_absolute_FT_features.append((fourier_coef[f]*fourier_coef[f].conjugate()).real)

#     return square_absolute_FT_features

# def get_Spectral_Power(segment):
#     delta = np.array(lfilter(bDelta, aDelta, segment))
#     theta = np.array(lfilter(bTheta, aTheta, segment))
#     alpha = np.array(lfilter(bAlpha, aAlpha, segment))
#     beta  = np.array(lfilter(bBeta, aBeta, segment))
#     gamma = np.array(lfilter(bGamma, aGamma, segment))

#     # SP computed using the variance of the filtered signal
#     # return [np.var(delta), np.var(theta), np.var(alpha), np.var(beta), np.var(gamma)]

#     return [np.trapz(delta), np.trapz(theta), np.trapz(alpha), np.trapz(beta), np.trapz(gamma)]

# def get_STFT(segment):
#     stft_coef = stft(np.array([segment]), sampling_frequency, 256) # 256 is the overlap, should be multiple of 2 < sample_freq

#     square_absolute_STFT_features = []

#     sampled_stft_coef = stft_coef[0][LOW_FREQ*SEGMENT_SIZE:HIGH_FREQ*SEGMENT_SIZE]
#     for i in range (len(sampled_stft_coef)):
#         for j in range (len(sampled_stft_coef[i])):
#             value = (sampled_stft_coef[i][j] * sampled_stft_coef[i][j].conjugate()).real
#             square_absolute_STFT_features.append(value)
#     return square_absolute_STFT_features

# def get_WELCH(segment):
#     f , welch_power_coef = signal.welch(segment, sampling_frequency, nperseg = sampling_frequency, 
#                                     scaling = 'spectrum')
#     return welch_power_coef[LOW_FREQ:HIGH_FREQ]

# def get_Interhemispheric_Power_Difference(SP_channel):
#     power_diff_matrix = []
#     ihpd_features = []

#     SETOFALLOWEDCHANNELS = constructChannelSet(CHOSENCHANNELS)

#     # assuming first 7 channels are on the left and the others are on the right
#     for l in range(7):
#         power_diff_matrix.append([])
#         if not l in SETOFALLOWEDCHANNELS:
#             continue
#         for r in range(7, 14):
#             power_diff_matrix[l].append([])
#             if not r in SETOFALLOWEDCHANNELS:
#                 continue
#             for f in range(len(SP_channel[0])):
#                 pow_diff_value = (SP_channel[l][f] - SP_channel[r][f]) / (SP_channel[l][f] + SP_channel[r][f])
#                 power_diff_matrix[l][r-7].append(pow_diff_value)
#                 ihpd_features.append(pow_diff_value)
#     return ihpd_features, power_diff_matrix
            
# def getInterhemispheric_Channel_Linear_Complexity(power_diff_matrix):
#     ihlc_features = []

#     # for each of the frequency bands
#     for f in range(len(power_diff_matrix)):
#         # Compute the variance-covariance matrix of the SPs
#         power_diff_matrix[f] = np.array(power_diff_matrix[f])
#         ones_vector = np.ones(7)
#         ones_matrix = np.outer(ones_vector, ones_vector.T)
#         deviation_scores_matrix = np.subtract(power_diff_matrix[f], np.matmul(ones_matrix, np.inner(power_diff_matrix[f], 1.0/7.0)))
#         deviation_scores_sums_matrix = np.matmul(deviation_scores_matrix.transpose(), deviation_scores_matrix)
#         covariance_matrix = np.inner(deviation_scores_sums_matrix, 1.0/7.0)
#         eignvalues = np.linalg.eigvals(covariance_matrix)
#         sum_eignvalues = np.sum(eignvalues)
#         zetas = []
#         for z in range(len(eignvalues)):
#             zetas.append(eignvalues[z]/sum_eignvalues)

#         exponent = 0.0
#         for z in range(len(zetas)):
#             exponent += zetas[z]*math.log10(zetas[z])
#         ihlc_features.append(math.exp(exponent))

#     return ihlc_features

# def prepare_for_cross_validation(subject_block_segment_feature, fold, number_test_blocks):
#     train_data  = []
#     train_class = []
#     test_data   = []
#     test_class  = []

#     for subject in range(len(subject_block_segment_feature)):
#         for block in range(len(subject_block_segment_feature[subject])):
#             if (block >= fold*number_test_blocks) and (block < fold*number_test_blocks+number_test_blocks):
#                 for segment in range(len(subject_block_segment_feature[subject][block])): 
#                     test_class.append(subject)
#                     test_data.append(subject_block_segment_feature[subject][block][segment])
#             else:
#                 for segment in range(len(subject_block_segment_feature[subject][block])): 
#                     train_class.append(subject)
#                     train_data.append(subject_block_segment_feature[subject][block][segment])

#     if fold < 0:
#         return train_data, train_class

#     return train_data, train_class, test_data, test_class

# def adjust_class_label(positive_class, class_labels):
#     new_class_labels = []
#     for i in range(len(class_labels)):
#         if class_labels[i] == positive_class:
#             new_class_labels.append(1)
#         else:
#             new_class_labels.append(-1)
#     return new_class_labels

# def constructChannelSet(chosenchannels):
#     chosenchannels = chosenchannels.split(' ')
#     if chosenchannels == ['']:
#         return set([0,1,2,3,4,5,6,7,8,9,10,11,12,13])
#     else:
#         chosenchannels = map(int, chosenchannels)
#         return set(chosenchannels)
